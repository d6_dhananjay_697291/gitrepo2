const mysql = require('mysql2')

const pool = mysql.createPool({
    host: "db",
    user: 'root',
    password: 'Manager',
    waitForConnections: true,
    connectionLimit: 20,
    database: 'movie_db',
})

module.exports = {
    pool,
}